from flask import request
from . import activity
from .. import db
from ..models import Activity, Keypoint
from datetime import date
import json

@activity.route('/create_activity', methods = ['GET', 'POST'])
def create_activity():
    if request.method == 'POST':
        context = request.get_json()
        print(context)
        activityname = context['activityname']
        username = context['username']
        activitydate = date(context['activitydate']['year'], context['activitydate']['month'], context['activitydate']['day'])
        activitydeaddate = date(context['activitydeaddate']['year'], context['activitydeaddate']['month'], context['activitydeaddate']['day'])
        activityintro = context['activityintro']
        activityattention = context['activityattention']
        new_activity = Activity(activityname = activityname,
                                username = username,
                                activitydate = activitydate,
                                activitydeaddate = activitydeaddate,
                                activityintro = activityintro,
                                activityattention = activityattention)
        db.session.add(new_activity)
        db.session.commit()
        index = 1
        keypoints = context['keypoint_list']
        while True:
            key = 'keypoint' + str(index)
            if(key in keypoints.keys()):
                latitude = keypoints[key]['latitude']
                longitude = keypoints[key]['longitude']
                type = keypoints[key]['type']
                new_keypoint = Keypoint(longitude = longitude,
                                        latitude = latitude,
                                        type = type,
                                        activity_id = new_activity.id)
                db.session.add(new_keypoint)
                db.session.commit()
                new_activity.keypoint.append(new_keypoint)
                index += 1
            else:
                break                        
        return 'Done!'
    return 'Use POST request!'