from . import db
from werkzeug.security import generate_password_hash, check_password_hash

class User(db.Model):
    # 定义表名
    __tablename__ = 'users'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(20), unique=True, index=True)
    password_hash = db.Column(db.String(128))

    def set_password(self, password):  # 用来设置密码的方法，接受密码作为参数
        self.password_hash = generate_password_hash(password)  # 将生成的密码保持到对应字段

    def validate_password(self, password):  # 用于验证密码的方法，接受密码作为参数
        return check_password_hash(self.password_hash, password)  # 返回布尔值

class Activity(db.Model):
    # 定义表名
    __tablename__ = 'activities'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    username = db.Column(db.String(20))
    activityname = db.Column(db.String(30), index=True)
    activitydate = db.Column(db.Date) # xxxx - xx - xx
    activitydeaddate = db.Column(db.Date) # xxxx - xx - xx
    activityintro = db.Column(db.String(256))
    activityattention = db.Column(db.String(256))
    # 关联属性
    keypoint = db.relationship("Keypoint", uselist=True, backref="activity", lazy='dynamic')

class Keypoint(db.Model):
    # 定义表名
    __tablename__ = 'keypoints'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    latitude = db.Column(db.DECIMAL(12,8))
    longitude = db.Column(db.DECIMAL(12,8))
    type = db.Column(db.Integer)
    # 外键
    activity_id = db.Column(db.Integer, db.ForeignKey(Activity.id))