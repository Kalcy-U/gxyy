from flask import request
from . import login_register
from .. import db
from ..models import User

@login_register.route('/create_account', methods = ['GET', 'POST'])
def create_account():
    if request.method == 'POST':
        name = request.form.get('name')
        password = request.form.get('password')
        new_user = User(name = name)
        new_user.set_password(password)
        db.session.add(new_user)
        db.session.commit()
        return 'Done!'
    return 'Use POST request!'