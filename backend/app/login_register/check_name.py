from flask import request
from . import login_register
from .. import db
from ..models import User

@login_register.route('/check_name', methods = ['GET', 'POST'])
def check_name():
    if request.method == 'POST':
        name = request.form.get('name')
         # 查询
        user_list = User.query.filter_by(name = name).all()
        if len(user_list) == 0:
            return 'Acceptable name!'
        else:
            return 'Already exists!'  
    return 'Use POST request!'   